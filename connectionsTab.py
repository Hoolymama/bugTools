import os.path
import pymel.core as pm
import pymel.core.uitypes as gui
import pymel.core.nodetypes as nt
import maya.mel as mel
from bugTools import utils 
import maya.cmds as mc
import re
import pymel.core.uitypes as gui

def selectionChanged(src, dest, origin):
	srcSel = pm.nodeOutliner(src, query=True, currentSelection=True)
	destSel = pm.nodeOutliner(dest, query=True, currentSelection=True)
	if origin == "src":
		if len(srcSel) > 0:
			pm.nodeOutliner(dest, edit=True, c=srcSel[0] )
		else:
			pm.nodeOutliner(dest, edit=True, redraw=True )

class ConnectionsTab(gui.FormLayout):

	def __init__(self):
		self.setNumberOfDivisions(100)
		self.createLoadButtons()
		self.createOutliners()
		self.createActionButtonsAndLayoutForm()


	def createLoadButtons(self):
		pm.setParent(self)
		self.loadSourceButton = pm.button( label="Load source", command=pm.Callback(self.loadObject, "source"))
		self.loadDestButton = pm.button( label="Load destination", command=pm.Callback(self.loadObject, "dest"))

	def somethingSelected(self):
		print "somethingSelected"

	def createOutliners(self):
		self.sourceOutliner = pm.nodeOutliner(
			showOutputs=True, showHidden=True,showNonKeyable=True,showReadOnly=True,
			addCommand=pm.Callback(self.clearOutlinerSelections) 
		)
 
		self.destOutliner = pm.nodeOutliner(
			showOutputs=True, showHidden=True,showNonKeyable=True,
			addCommand=pm.Callback(self.clearOutlinerSelections) 
		)

		# THIS WORKS - but its a hack - go through mel and back to python
		# because selectCommand flag doesn't work it seems
		pyCmd =  "callPython(\"bugTools\", \"connectionsTab.selectionChanged\", {\""+self.sourceOutliner+"\" , \""+self.destOutliner+"\"  , \"src\"})"
		pyCmd= pm.encodeString(pyCmd) 
		cmd = ('nodeOutliner -e -selectCommand "'+pyCmd+'" ' + self.sourceOutliner)
		mel.eval(cmd)
		pyCmd =  "callPython(\"bugTools\", \"connectionsTab.selectionChanged\", {\""+self.sourceOutliner+"\" , \""+self.destOutliner+"\"  , \"dest\"})"
		pyCmd= pm.encodeString(pyCmd) 
		cmd = ('nodeOutliner -e -selectCommand "'+pyCmd+'" ' + self.destOutliner)
		mel.eval(cmd)

	def loadObject(self, which):
		sel=pm.ls(selection=True)
		if (len(sel) == 0):
			pm.displayError("Nothing Selected")
		ob = sel[0]
		ol = self.sourceOutliner
		if which=="dest":
			ol = self.destOutliner
		pm.nodeOutliner(ol, edit=True, rpl=ob) 

	def clearOutlinerSelections(self):
		print "clearOutlinerSelections"


	def connectPairs(self, src, dest):
		if   len(src) !=  len(dest):
			pm.displayError("Something wrong with array lengths - sorry")
			return
		n =  len(src)
		for i in range(n):
			if not pm.isConnected( src[i], dest[i] ):
				print ("connecting "+ src[i] +" => "+ dest[i] )
				pm.Attribute(src[i]) >>  pm.Attribute(dest[i])
			else:
				pm.displayWarning(src[i] +" is already connected to "+ dest[i])

	def disconnectPairs(self, src, dest):
		if   len(src) !=  len(dest):
			pm.displayError("Something wrong with array lengths - sorry")
			return
		n =  len(src)
		for i in range(n):
			if pm.isConnected( src[i], dest[i] ):
				print ("disconnecting "+src[i] +" => "+ dest[i])
				pm.Attribute(src[i]) //  pm.Attribute(dest[i])
			else:
				pm.displayWarning(src[i] +" is not connected to "+ dest[i])
				


	def connect(self, mode):
		srcSel = pm.nodeOutliner(self.sourceOutliner, query=True, currentSelection=True)
		destSel = pm.nodeOutliner(self.destOutliner, query=True, currentSelection=True)
		if not  (srcSel  and   destSel):
			pm.displayError("Select an attribute from each panel")
			return
		srcSel = self.expand(srcSel[0])
		destSel = self.expand(destSel[0])
		numSrc = len(srcSel)
		numDest = len(destSel)
		if numDest > numSrc:
			srcSel = [srcSel[0]] * numDest
		elif (numSrc > numDest) and (numDest == 1):
			srcSel = [srcSel[0]]
		if mode:
			self.connectPairs(srcSel, destSel)
		else:
			self.disconnectPairs(srcSel, destSel)
 
	def expand(self, nodeAttr):
		result = []
		pattern = re.compile('^beetleRef_[0-9]+:.*$') 
		if pattern.match(str(nodeAttr)):
			for ns in utils.getBeetleNamespaces():
				result.append(re.sub('beetleRef_[0-9]+', (ns+":") , nodeAttr))
		else:
			result.append(nodeAttr)
		return result

	def disconnect(self):
		print "disconnect"
	

	def createActionButtonsAndLayoutForm(self):
		pm.setParent(self) #form
		connectButton = pm.button( label='Connect', command=pm.Callback(self.connect, True) )
		disconnectButton = pm.button( label='Disconnect', command=pm.Callback(self.connect, False) )
 
		self.attachForm(self.loadSourceButton, 'top', 2)
		self.attachForm(self.loadSourceButton, 'left', 2)
		self.attachPosition(self.loadSourceButton, 'right', 2, 50)
		self.attachNone(self.loadSourceButton, 'bottom')
		
		self.attachForm(self.loadDestButton, 'top', 2)
		self.attachForm(self.loadDestButton, 'right', 2)
		self.attachPosition(self.loadDestButton, 'left', 2, 50)
		self.attachNone(self.loadDestButton, 'bottom')
		
		self.attachControl(self.sourceOutliner, 'top', 2, self.loadSourceButton)
		self.attachForm(self.sourceOutliner, 'left', 2)
		self.attachPosition(self.sourceOutliner, 'right', 2, 50)
		self.attachControl(self.sourceOutliner, 'bottom', 2, disconnectButton)
		
		self.attachControl(self.destOutliner, 'top', 2, self.loadDestButton)
		self.attachForm(self.destOutliner, 'right', 2)
		self.attachPosition(self.destOutliner, 'left', 2, 50)
		self.attachControl(self.destOutliner, 'bottom', 2,  connectButton)
 
		self.attachNone(disconnectButton, 'top'  )
		self.attachPosition(disconnectButton, 'right', 2, 50)
		self.attachForm(disconnectButton, 'left', 2)
		self.attachForm(disconnectButton, 'bottom', 2)

		self.attachNone(connectButton, 'top'  )
		self.attachPosition(connectButton, 'left', 2, 50)
		self.attachForm(connectButton, 'right', 2)
		self.attachForm(connectButton, 'bottom', 2)
