import os.path
import pymel.core as pm
import pymel.core.uitypes as gui
import pymel.core.nodetypes as nt
import maya.mel as mel
from bugTools import utils 
import maya.cmds as mc
import re
import pymel.core.uitypes as gui
 
class MiscTab(gui.FormLayout):

	def __init__(self):
		self.setNumberOfDivisions(100)
		self.column = pm.columnLayout(width=300)
		self.column.adjustableColumn(True)
		self.createButtons()
 
	def createButtons(self):
		pm.setParent(self.column)
		self.selectSameButton = pm.button( label="Select same part", command=pm.Callback(self.selectSamePart))

		self.showLodRow = pm.rowLayout( 
			numberOfColumns=2, 
			columnWidth2=(150, 150 ), 
			adjustableColumn=1, 
			columnAlign=(1, 'right'), 
			columnAttach=[(1, 'both', 0), (2, 'both', 0)   ] 
		)
 
		pm.button( label='Show low-res',
			command=pm.Callback(self.showHighRes, False)
		)
		pm.button( label='Show high-res' ,
			command=pm.Callback(self.showHighRes, True)
		)

	def selectSamePart(self):
		result = []
		sel = pm.ls(selection=True)
		for ns in utils.getBeetleNamespaces():
			for o in sel:
				name = re.sub('beetleRef_[0-9]+:', (ns+":") , str(o))
				result.append(name)
		pm.select(result)
    
	def showHighRes(self, which):
		for o in pm.ls("*:*High",type="transform"):
			o.visibility.set(which)
		for o in pm.ls("*:*Low",type="transform"):
			o.visibility.set(not which)
 