import pymel.core as pm
import pymel.core.datatypes as dt
import pymel.core.uitypes as gui
from bugTools import colorSample as cs
from bugTools import utils 

def getAllChannelNames():
	names = []
	ns = "beetleRef_00"
	shaders = pm.ls( ns+":*:SS" )
	for shader in shaders:
		name = shader.split(":")[1]
		names.append(name)
	return names
	
def getShadingNodes(name):
	return pm.ls( "*:"+name+":SS" )

def getShadingGroups(name):
	return pm.ls( "*:"+name+":SG" )

def getExpressions(name):
	return pm.ls( "*:"+name+":hsvExpr" )

def getHueCurves(name):
	return pm.ls( "*:"+name+":hueCurve" )

def getSatCurves(name):
	return pm.ls( "*:"+name+":satCurve" )

def getValCurves(name):
	return pm.ls( "*:"+name+":valCurve" )

def getRootRGBs(plug):
	points = utils.getRootPositions()
	colors = cs.sampleColors(plug, points)
	return  colors
 
def attachSamplePads():
	for root in utils.getRoots():
		ns = root.split(":")[0]
		padname = (ns+":samplePad")
		if not pm.objExists(padname):
			res = pm.polyPlane(n=padname, sx=1, sy=1, w=1, h=0.5)
			pad = res[0]
			pad.attr('t').set((0,0.5,0))
			pm.parent(pad,root,r=True)


def getOrCreateSamplePads():
	samplePads = pm.ls( "*:samplePad")
	if (len(samplePads) != utils.numBeetles()):
		if len(samplePads) > 0:
			pm.delete( "*:samplePad")
		attachSamplePads()
	return pm.ls( "*:samplePad")


def createHsvExpr(ns, name, shader):
	hueCurve = pm.createNode('animCurveTU',  name=(ns + ':' + name+":hueCurve"))
	satCurve = pm.createNode('animCurveTU',  name=(ns + ':' + name+":satCurve"))
	valCurve = pm.createNode('animCurveTU',  name=(ns + ':' + name+":valCurve"))
	expr = ("float $hue  =  fmod(  " +ns + ':' + name+":hueCurve.output, 1.0);\n")
	expr += ("float $sat  =  " +ns + ':' + name+":satCurve.output;\n")
	expr += ("float $val  =  " +ns + ':' + name+":valCurve.output;\n")
	expr += ("float $col[] = hsv_to_rgb(<<$hue, $sat, $val>>);\n")
	expr += (ns +":"+ name +":SS.outColorR = $col[0];\n")
	expr += (ns +":"+ name +":SS.outColorG = $col[1];\n")
	expr += (ns +":"+ name +":SS.outColorB = $col[2];\n")
	return pm.expression(s=expr,  name=(ns + ':' + name+":hsvExpr"))

def switchChannel(name):
	for pad in getOrCreateSamplePads():
		ns = pad.split(":")[0]
		sg = (ns + ':' + name+":SG")
		pm.sets(sg, edit=True, forceElement=pad)
		pm.currentTime(pm.currentTime()) #refresh 

def createNamespacedShaders(name):
	shaderNodes = []
	for pad in getOrCreateSamplePads():
		ns = pad.split(":")[0]
		ss = pm.shadingNode('surfaceShader', asShader=True, name=(ns + ':' + name+":SS"))
		sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True,  name=(ns + ':' + name+":SG"))
		ss.attr('outColor') >> sg.attr('surfaceShader')
		createHsvExpr(ns, name, ss)
		pm.sets(sg, edit=True, forceElement=pad)
		shaderNodes.append(ss)
	return shaderNodes

def getOrCreateShaderNodes(name):
	shaderNodes = pm.ls( "*:"+name+":SS" )
	if (len(shaderNodes) == 0):
		shaderNodes = createNamespacedShaders(name)
	return shaderNodes

def deleteNamedChannel(name):
	pm.delete(getShadingGroups(name))
	pm.delete(getShadingNodes(name))
	pm.delete(getExpressions(name))
	pm.delete(getHueCurves(name))
	pm.delete(getSatCurves(name))
	pm.delete(getValCurves(name))

def createNamedChannel(name):
	shaders = getOrCreateShaderNodes(name)
	pm.currentTime(pm.currentTime()) 

def keySampledColors(plug, name):
	shaders = getOrCreateShaderNodes(name)
	rgbs = getRootRGBs(plug)
	hueCrvs = getHueCurves(name)
	satCrvs = getSatCurves(name)
	valCrvs = getValCurves(name)
	n = utils.numBeetles()
	frame = int(pm.currentTime(query=True))
	for i in range(n):
		hsv = dt.Color.rgbtohsv(rgbs[i])
		pm.setKeyframe(hueCrvs[i],   t=frame, v=hsv[0] )
		pm.setKeyframe(satCrvs[i],   t=frame, v=hsv[1] )
		pm.setKeyframe(valCrvs[i],   t=frame, v=hsv[2] )
	pm.currentTime(pm.currentTime()) #refresh 

def colorPlugFromSelection():
	try:
		mesh = pm.ls( selection=True, dag=True, type="mesh" )[0]
		se = pm.listConnections(mesh, t="shadingEngine")[0]
		ss = pm.listConnections(se+".surfaceShader")[0]
		return (ss+".outColor")
	except:
		pm.displayError('Select one mesh that has a shader attached.')


 


