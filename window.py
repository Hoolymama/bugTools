import os.path
import pymel.core as pm
import pymel.core.uitypes as gui
from  bugTools import core 
from  bugTools import beetleColorTab 
from  bugTools import nameSpaceTab 
from  bugTools import connectionsTab 
from  bugTools import miscTab 

class BugToolsWindow(gui.Window):

	def __init__(self):
		others = pm.lsUI(windows=True)
		for win in others:
			if pm.window(win, q=True, title=True) == "Bug Tools":
				pm.deleteUI(win)
		    
		self.setTitle('Bug Tools')
		self.setIconName('Bug Tools')
		self.setWidthHeight([500, 500])

		self.tabs = pm.tabLayout()
		pm.setParent(self.tabs)

		self.beetleColorTab = beetleColorTab.BeetleColorTab()
		self.tabs.setTabLabel((self.beetleColorTab, "Colorize"))

		pm.setParent(self.tabs)
		self.namespaceTab = nameSpaceTab.NamespaceTab()
		self.tabs.setTabLabel((self.namespaceTab, "Rename"))

		pm.setParent(self.tabs)
		self.connectionsTab = connectionsTab.ConnectionsTab()
		self.tabs.setTabLabel((self.connectionsTab, "Connect"))

		pm.setParent(self.tabs)
		self.miscTab = miscTab.MiscTab()
		self.tabs.setTabLabel((self.miscTab, "Misc"))

		self.show()
		self.setResizeToFitChildren()
		self.tabs.setSelectTabIndex(1)

	def onTabChanged(self):
		print ""
 