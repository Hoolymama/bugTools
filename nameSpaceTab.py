import os.path
import pymel.core as pm
import pymel.core.uitypes as gui
import pymel.core.nodetypes as nt

from bugTools import utils 

import pymel.core.uitypes as gui

class NamespaceTab(gui.FormLayout):

	def __init__(self):
		self.setNumberOfDivisions(100)
		pm.setParent(self)
		self.createHeaderRow()
		pm.setParent(self)
		self.scroll = pm.scrollLayout(childResizable=True)
		self.column = pm.columnLayout( )
		self.column.adjustableColumn(True)
		self.createNameRows()
		self.createActionButtons()
 
	def createNameRows(self):
		namespaces = utils.getBeetleNamespaces()
		for ns in namespaces:
			self.createNameRow(ns)

	def createNameRow(self, namespace):
		pm.setParent(self.column)
		r = pm.rowLayout( 
			numberOfColumns=3, 
			columnWidth3=(100, 75, 75), 
			adjustableColumn=2, 
			columnAlign=(1, 'left'), 
			columnAttach=[(1, 'both', 0), (2, 'both', 0) , (3, 'both', 0) ] 
		)
		text = pm.text( label=namespace)
		nf = pm.nameField(enable=False)
		bt = pm.button( label="Load one", command=pm.Callback(self.loadSelected, nf))

	def createHeaderRow(self):
		pm.setParent(self)
		self.headerRow = pm.rowLayout( 
			numberOfColumns=3, 
			columnWidth3=(100, 75, 90), 
			adjustableColumn=2, 
			columnAlign=(1, 'left'), 
			columnAttach=[(1, 'both', 0), (2, 'both', 0) , (3, 'both', 0) ] 
		)
		pm.setParent(self.headerRow)
		text = pm.text( label="New tail name")
		self.tailTextField = pm.textField()
		bt = pm.button( label="Load selected", command=pm.Callback(self.loadSelected))
 
	def loadSelected(self, nameField=None): 
		if nameField != None:
			sel = pm.ls(selection=True)
			if (len(sel)):
				gui.NameField(nameField).setObject(sel[0])
			else:
				gui.NameField(nameField).setObject()
			return
		namefields = list(map(
			(lambda row:  gui.RowLayout(row).getChildArray()[1] ), 
			self.column.getChildArray()
			))
		sel = pm.ls(selection=True)
		i=0
		n = len(sel)
		for i in range(utils.numBeetles()):
			if i < n:
				gui.NameField(namefields[i]).setObject(sel[i])
			else:
				gui.NameField(namefields[i]).setObject()
			i+=1

	def renameWithNamespace(self):
		tail = self.tailTextField.getText( )
		if (len(tail) < 1):
			pm.displayError('Must enter a name') 
			return
		for row in self.column.getChildArray():
			row = gui.RowLayout(row)
			namespace = pm.text(row.getChildArray()[0], query=True, label=True) 
			obj =  gui.NameField(row.getChildArray()[1]).getObject()
			if obj:
				pm.rename(obj, (namespace+":"+tail)) 
			else:
				print "no object to rename with "+ namespace
 
	def createActionButtons(self):
		pm.setParent(self) #form
		keyButton = pm.button( label='Rename', command=pm.Callback(self.renameWithNamespace) )
		self.attachForm(self.headerRow, 'top', 2)
		self.attachForm(self.headerRow, 'left', 2)
		self.attachForm(self.headerRow, 'right', 2)
		self.attachNone(self.headerRow, 'bottom')
		
		self.attachNone(keyButton, 'top'  )
		self.attachForm(keyButton, 'left', 2)
		self.attachForm(keyButton, 'right', 2)
		self.attachForm(keyButton, 'bottom', 2)

		self.attachForm(self.scroll, 'left', 2)
		self.attachForm(self.scroll, 'right', 2)
		self.attachControl(self.scroll, 'top', 2, self.headerRow)
		self.attachControl( self.scroll, 'bottom', 2, keyButton)  
	