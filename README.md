# BugTools #

BugTools contains: 

* A tool to set color channels for the bug colony. 
* A tool to rename objects with namespaces matching the bugs, i.e. *beetleRef_(number):*. 
* A tool to connect and disconnect attributes for all the colony.
* A panel of miscellaneous functions.
 
This README will also cover some workflow stuff related to the scene packages.

## Installation

* Download from https://gitlab.com/Hoolymama/bugTools. There is a download button, or use the direct link below.

```
https://gitlab.com/Hoolymama/bugTools/repository/master/archive.zip
```


* Rename folder to bugTools and put it inside your maya scripts directory

```
\Documents and Settings\<username>\My Documents\maya\scripts\bugTools
```

Alternatively, you can use `git` which will allow you to update easily by typing `git pull`

To make sure it installed okay, open Maya and enter the following commands in the script editor (python):

```
from bugTools import window as bw
bw.BugToolsWindow()
```

Note: The above command is slightly different than before. If you made a shelf button you should replace it.

You should see a window pop up. *If not, then check the location and name of the bugTools folder.* 
You can drag those commands to the shelf to make a button for later.


## Color Sampler

The purpose of the color sampler tool is to set keyframes for the colors of all the beetles at once by sampling a projection texture based on the beetle's location. The colors are applied to a named channel, for example **SPEC**, or **WING_TRANSPARENCY**

### Get Started

To create an animated color channel:

* Open a scene with beetles in it and go to textured mode in the viewport.
* Create a flat plane and assign a surface shader with a projection texture mapped to its color so that you see some pattern from above.
* Open the bugTools UI (use commands from above or the shelf button) and select the Colorize tab.
* Select the plane and then hit the button that says **Set from mesh** in the Sample plug row. It should be filled in with something like: `surfaceShader1.outColor`.
* Choose a new name for the channel, e.g. COLOR, and make sure it shows in the Channel namespace option menu. At this point you'll notice each beetle has a little black plane hovering above it. These are called sample pads.
* Go to a frame where the beetle arrangement puts them at the right place in the projection texture.
* Press **Set key**.

Now the sample pads have take the color of the projection texture. Note, the projection shader is not assigned to them. They have been assigned separate shaders and the color channel has been keyed with the value according to their location when you hit the button. If you now scrub through, those colors will remain with the beetles as they wander about.

* Go to the next formation
* Change the colors and other attributes of the projection texture however you want. If you use a completely different shader you'll need to hit the 
**Set from mesh** button again.
* When you are happy, hit Set key again.

### HSV animation

If you scrub between the two keys you'll see the colors change for each beetle. The animation is applied to HSV components and converted to RGB. In this way, as the colors interpolate from say bright blue to bright red, the color will travel around the color wheel and remain saturated, probably purple.  
 
### Set more channels

To create another channel, for example SPEC, use the UI to create a new Channel namespace. You'll need to select it in the dropdown menu. (I'll make this automatic in a future version). You'll notice the sample pads are assigned a new shader representing the new channel.

## Rename

The Rename tab allows you to name many objects with a single name and have namespaces applied to match the beetles. When objects are named this way the scene is easier to understand with and easier to automate.

To rename objects:

* Select some objects or shading nodes, ideally the same number as there are beetles.
* Hit the Load selected button. They will fill up the fields next to the namespace labels.
* Enter a tail name, for example ColorRamp.
* Hit the Rename button.

Now the objects will be named beetleRef_00:ColorRamp, beetleRef_01:ColorRamp ...  and so on.

## Connect

The Connect tab, like the Rename tab, is designed to make it easier to work with a colony all at once. You use it to connect and disconect plugs. The idea is you load one object in each side, and if the objects are in a beetleRef namespace such as beetleRef_01, then the connections will be performed for the equivalent objects under the other beetleRef namespaces.

* **Many to many** If both the destination and source objects are in a beetleRef namespace then the connections will be made for the each pair in all beetleRef namespaces.

*  **One to many** If the destination object is in a beetleRef namespace and the source object is not, then connections will be made between the source object and all destination objects.

*  **Many to one** If the source object is in a beetleRef namespace and the destination object is not, then no connections will be made because a single destination plug may not have many sources unless it is a multi, and multi plugs are not supported at this time.

*  **One to one** If neither the destination or source objects are in a beetleRef namespace then only a single connection will be made, just the same as using the standard Maya connect window.

## Miscellaneous

The Miscellaneous tab is a dumping ground for useful scripts. 

## Scene Package

The package is named something like: `export_14_09_17_2115` and contains all files needed to see the results of a sim. However, most of the time you'll only need to update the cache, or possibly the beetle reference (if its version number changes).
Structure is something like this:

```
|____cache
| |____alembic
| | |____beetles__dev__sim__009__022__mexicanWave.abc
|____scenes
| | |____beetleColony_14_09_17_2115.ma.swatches
| |____beetleColony_14_09_17_2115.ma
| |____riggedBeetleK.0013.ma
```

Files should be moved to the equivalent locations in your maya project.
 
