import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaRender as omr

def sampleColors(plug, points):
  numSamples = len(points)
  floatPoints = om.MFloatPointArray()
 
  for i in range(len(points)):
    floatPoints.append(om.MFloatPoint(points[i][0], points[i][1], points[i][2]))

  resultColors = om.MFloatVectorArray()
  resultTransparencies = om.MFloatVectorArray()
 
  omr.MRenderUtil.sampleShadingNetwork(
    plug, 
    numSamples, 
    False, 
    False, 
    om.MFloatMatrix(), 
    floatPoints, 
    None, 
    None, 
    None, 
    floatPoints, 
    None, 
    None, 
    None, 
    resultColors, 
    resultTransparencies )

  result = []
  for i in range(resultColors.length()):
    col= om.MFloatVector(resultColors[i])
    result.append([col.x, col.y, col.z])
  return result;
