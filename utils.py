import pymel.core as pm
def getRootPositions():
	pointArray = []
	for root in getRoots():
		pointArray.append(pm.xform(root, query=True, ws=True, rp=True))
	return pointArray

def getBeetleNamespaces():
	return list(map((lambda b: b.split(":")[0]), pm.ls("*:beetle" )))

def getTopNodes():
	return pm.ls("*:beetle" )

def getRoots():
	return pm.ls("*:beetleRoot" )

def numBeetles():
	return len(getTopNodes())
