import os.path
import pymel.core as pm
from  bugTools import beetleColor 
import pymel.core.uitypes as gui

class BeetleColorTab(gui.FormLayout):

	def __init__(self):
		self.setNumberOfDivisions(100)
		pm.setParent(self)
		self.column = pm.columnLayout( )
		self.column.adjustableColumn(True)
		self.createChannelNameRow()
		self.createColorPlugRow()
		self.createActionButtons()
		self.populateChannelMenu() 


	def createChannelNameRow(self):
		pm.setParent(self.column)
		self.channelNameRow = pm.rowLayout( 
			numberOfColumns=3, 
			columnWidth3=(250, 75, 75), 
			adjustableColumn=1, 
			columnAlign=(1, 'right'), 
			columnAttach=[(1, 'both', 0), (2, 'both', 0) , (3, 'both', 0) ] 
		)
		self.channelMenu = pm.optionMenu( label='Channel namespace', changeCommand=self.onChangeChannelName)
		pm.button( label='New', height=18,
			command=pm.Callback(self.on_new_channel_click)
		)
		pm.button( label='Delete', height=18,
			command=pm.Callback(self.on_delete_channel_click)
		)
 
	def onChangeChannelName(self, name):
		beetleColor.switchChannel(name)

	def createColorPlugRow(self):
		pm.setParent(self.column)
		self.colorPlugRow = pm.rowLayout( 
			numberOfColumns=2, 
			columnWidth2=(150, 150), 
			adjustableColumn=1, 
			columnAlign=(1, 'right'), 
			columnAttach=[(1, 'both', 0), (2, 'both', 0)  ] 
		)
		self.colorPlugField = pm.textFieldGrp(
			columnWidth2=(105  , 70), 
			adjustableColumn=2 ,
			label='Sampler plug' )
		pm.button( 
			label='Set from mesh',
			height=18 ,  
			command=pm.Callback(self.on_set_plug_click))

	def createActionButtons(self):
		pm.setParent(self) #form
		keyButton = pm.button( 
			label='Set key',
			command=pm.Callback(self.on_set_key_click))

		self.attachNone(  keyButton, 'top'  )
		self.attachForm(keyButton, 'left', 2)
		self.attachForm(keyButton, 'right', 2)
		self.attachForm(keyButton, 'bottom', 2)

		self.attachForm(self.column, 'left', 2)
		self.attachForm(self.column, 'right', 2)
		self.attachForm(self.column, 'top', 2)
		self.attachControl( self.column, 'bottom', 2, keyButton)  
		# self.attachForm([
		# 	(keyButton, 'left', 2),
		# 	(keyButton, 'right', 2),
		# 	(keyButton, 'bottom', 2) ,
		# 	(self.column, 'left', 2),
		# 	(self.column, 'right', 2),
		# 	(self.column, 'top', 2)  	])
		# self.attachControl([ (self.column, 'botttom', 2, keyButton) ])
 
	def getCurrentChannelName(self):
		name = pm.optionMenu(self.channelMenu, query=True, value=True)
		if name == "":
			pm.displayError("Must have a valid channel name: ("+name+")" )
		else:
			return name

	def getCurrentPlug(self):
		plug = pm.textFieldGrp(self.colorPlugField, query=True, text=True)
		if not pm.objExists(plug):
			pm.displayError('Must have a valid plug to sample: ('+ plug +")" )
		else:
			return plug

	def on_set_key_click(self):
		plug = self.getCurrentPlug()
		name = self.getCurrentChannelName()
		beetleColor.keySampledColors(plug, name)
 
	def on_delete_channel_click(self):
		name = self.getCurrentChannelName()
		result = pm.confirmDialog( title='Confirm', message='Delete '+name+'?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
		if result == "Yes":
			beetleColor.deleteNamedChannel(name)
			self.populateChannelMenu()

	def populateChannelMenu(self):
		itemList = pm.optionMenu(self.channelMenu, query=True , itemListLong=True)
		for item in itemList:
			pm.deleteUI(item)
		names = beetleColor.getAllChannelNames()
		for name in names:
			pm.menuItem( label=name, parent=self.channelMenu )

	def on_set_plug_click(self):
		result = beetleColor.colorPlugFromSelection()
		pm.textFieldGrp(self.colorPlugField, edit=True, text=result)  
	
	def on_new_channel_click(self):
		result = pm.promptDialog(
			title='New Channel Name',
			message='Enter Name:',
			button=['OK', 'Cancel'],
			defaultButton='OK',
			cancelButton='Cancel',
			dismissString='Cancel')
		if result == 'OK':
			text = pm.promptDialog(query=True, text=True)
			beetleColor.createNamedChannel(text)
			pm.menuItem( label=text, parent=self.channelMenu )
#  